#include <iostream>
#include <string>
using namespace std;

const int version = 0;

enum state { //custom three state boolean variable
	NULL_STATE = -1,
	FALSE,
	TRUE
};

bool board_checkfull(enum state board[3][3]) { //check if 3x3 array of state variables (game board) has free space
	for (int i = 0; i < 3; i++){ 
		for (int j = 0; j < 3; j++){
			if ( board[i][j] == -1 ) {
			return false;
			}
		}
	}
	return true;
}

bool board_checkwin(enum state board[3][3]) { //check if there is line of three characters in game board
	if (( board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[2][2] != -1 ) || ( board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[2][0] != -1 )) {
		return true;
	}
	for (int i = 0; i < 3; i++){ 
		if ( ( board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][2] != -1 ) || ( board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[2][i] != -1 ) ) {
			return true;
		}

	} //yes, it just checks all of possible lines, I'm too stupid to make something more smart (I could use the same but with more cycles If I haven't decided to hardcode 3O3 board)
	return false;
}

void board_print(enum state board[3][3]) { //prints game board
	for (int i = 0; i < 3; i++){ 
		for (int j = 0; j < 3; j++){
			switch ((int)board[i][j]) {
				case 1:
					cout << "  [X]";
					break;
				case 0:
					cout << "  [O]";
					break;
				case -1:
					cout << "  [ ]";
					break;
			}
		}
		cout << "\n";
	}
}

int main(int argc, char *argv[]) {
	if (argc > 1){
	string argv1 = argv[1];
	if(argv1 == "-h" or argv1 == "--help"){
		cout << "Classic 3x3 tic tac toe" << "\n";
		cout << "Take turns && make a horisontal or diagonal line with 3 of your symbols" << "\n";
		cout << "You cannot place on top of existing symbol" << "\n";
		cout << "Input format is row and column numbers (1-3) separated by space" << "\n";
		cout << "" << "\n";
		cout << "" << "\n";
		
	};
	}
	cout << "Tiny tick-tack-toe v" << version << "\n";
	enum state board[3][3]; //game board

	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			board[i][j] = NULL_STATE; //haven't thought of better way to fill it with NULL_STATES
		}
	}
	
	board_print(board); //first print of game board, originally as test but left here to show empty board

	bool turn = true; //turn variable is a boolean, true is V false is O
	
	while (true){
		char x_O;
		if (turn){
			x_O = 'X';
		}
		else{
			x_O = 'O';
		}
		cout << x_O << " position: ";
		int i;
		int j;
		cin >> i >> j;
		i--;
		j--; // transform i && j from 1-based numbers to 0-based as needed by array
		if (i >= 0 && i <= 2 && j >= 0 && j <= 2) {
			if (board[i][j] == -1) {
				board[i][j] = (enum state)turn; //turn is bool, so i can be transformed to 1/0 value && used as custom state variable
				board_print(board);
				turn = !turn;
				if ( board_checkwin(board) ) { cout << x_O << " won!"; return 0;}	
				if ( board_checkfull(board) ) { cout << "No one won, I won :>"; return 0;}
			}
			else {cout << "you cannot place over existing crosses && nulls\n";}
		}
		else { cout << "invalid input, you cannot use rows && columns with numbers smaller than 1 or bigger than 3\n";}
		
	} 
	
	return 0;
} 
