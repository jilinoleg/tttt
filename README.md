# Tiny tic-tac-toe

Tiny tic-tac-toe is a small cli tic-tac-toe game written in C++.

It could have been better.

It could have been in C.

But it's actually by first attempt at c++ game and it works (!). I plan to use it as base for several next projects (trying out GUI frameworks and making a tic tac toe 2), but it will require me to rewrite some hardcoded code to either be hardcoded for these new projects or to finally be separated into more or less universal functions, so it is possible that I will update this piece of code.

# Build

`g++ tttt.cpp -o tttt -O`

# Run
 
`./tttt` to start game

`./tttt -h` or `./tttt --help` to view small help text and then start game

# Help (same as -h flag)
```
Classic 3x3 tic tac toe
Take turns && make a horisontal or diagonal line with 3 of your symbols
You cannot place on top of existing symbol
Input format is row and column numbers (1-3) separated by space
```

